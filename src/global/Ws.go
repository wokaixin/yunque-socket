package global

import (
	"src/connection"
)

type FailToken struct {
	//当前失败次数
	FailNum int
	//上次失败时间
	LastTimep int64
}

// ip 黑名单 配置
type IpBlacklistCfg struct {
	//# ip 黑名单链接token 连续失败次数
	FailMaxNum int
	//ip 黑名单失败间隔记录时间
	FailIntervalTime int64
}

type Ws struct {
	ConnList map[string]*connection.Ws
	ConnInfo
}

var Ws_ *Ws

func WsInit() {
	Ws_ = WsNew()
}
func WsNew() *Ws {

	Ws_ := &Ws{make(map[string]*connection.Ws), *NewConnInfo()}
	//ws在线链接
	Ws_.ConnList = make(map[string]*connection.Ws)
	return Ws_
}

func (*Ws) Get() *Ws { return Ws_ }

// 唯一id自增
func (*Ws) Inc() *Ws {
	Ws_.Id++
	return Ws_
}

func (this *Ws) GetConnList() map[string]*connection.Ws {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	return this.ConnList
}
func (this *Ws) GetConnListVal(key string) *connection.Ws {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	return this.ConnList[key]
}

func (this *Ws) UpdateConnList(key string, value *connection.Ws) {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	this.ConnList[key] = value
}

func (this *Ws) DelConnList(key string) {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	if this.ConnList[key] != nil {
		delete(this.ConnList, key)
	}
}
