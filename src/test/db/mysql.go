package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql" // 注意下划线，它会触发包的初始化，注册驱动
	"log"
)

func main() {
	user := "g_g_01film"
	pass := "TtnWAzw35kHMPxhT"
	dbname := "g_g_01film"
	mysqlUrl := user + ":" + pass + "@tcp(127.0.0.1:3306)/" + dbname

	db, err := sql.Open("mysql", mysqlUrl)
	if err != nil {
		log.Fatalln(err)
		panic(err)
	}
	// 注意这行代码要写在上面err判断的下面
	defer db.Close()
}
