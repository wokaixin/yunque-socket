/**
* @author: yichen
* @Date: 2023/9/14 0014
* @Description: 向多用户发消息模版

* @return:
    **/

package model

import "src/model/ProtoModel"

type SubMessageModel struct {
	//服务器id 可选
	ServiceId string
	//用户id 可选
	Uid string
	//
	Cmd     byte
	Message ProtoModel.MessageProto
}
