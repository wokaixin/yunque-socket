package model

type ConnUserinfoModel struct {
	Token       string
	Channel     string
	AppVersion  string
	OsVersion   string
	PackageName string
	DeviceId    string
	DeviceName  string
	Language    string
}
