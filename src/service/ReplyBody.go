package service

import (
	"encoding/json"
	"google.golang.org/protobuf/proto"
	"log"
	"src/config"
	global2 "src/global"
	"src/model/ProtoModel"
)

type ReplyBody struct {
}

func GetReplyBody() *ReplyBody {
	return &ReplyBody{}
}

// 向指定链接推送消息
func (this *ReplyBody) SendAll(data []byte) {
	(&Service{}).SendAll(ProtoModel.DataTypeProto_REPLY, data)
}

// 向指定链接推送消息
func (this *ReplyBody) Send(data []byte, connId string) {
	(&Service{}).Send(ProtoModel.DataTypeProto_REPLY, data, connId)
}

// 向指定链接推送消息
func (this *ReplyBody) SendNew(data []byte, connId string, tp string) {
	(&Service{}).SendNew(ProtoModel.DataTypeProto_REPLY, data, connId, tp)
}

// 向指定链接推送消息
func (this *ReplyBody) SendUid(data []byte, uid string) {
	(&Service{}).SendUid(ProtoModel.DataTypeProto_REPLY, data, uid)
}

// 向指定链接推送消息
func (this *ReplyBody) SendUids(data []byte, uids []string) {
	(&Service{}).SendUids(ProtoModel.DataTypeProto_REPLY, data, uids)
}

// 向指定链接推送消息
func (this *ReplyBody) SendUidDid(data []byte, uid string, Did string) {
	(&Service{}).SendUidDid(ProtoModel.DataTypeProto_REPLY, data, uid, Did)
}
func (this *ReplyBody) Marshal(replyBodyProto *ProtoModel.ReplyBodyProto) ([]byte, string) {
	msgFmt := config.V().GetString("client.msg-fmt")
	if msgFmt == "proto" {
		res, err := proto.Marshal(replyBodyProto)
		if err != nil {
			// 格式化失败
			log.Println(err)
			return nil, msgFmt
		}
		return res, msgFmt
	} else {
		// 转json
		res, err := json.Marshal(replyBodyProto)
		//fmt.Println("data.Unmarshal:", string(res))
		//messageProto.String()
		if err != nil {
			// 格式化失败
			log.Println(err)
			return nil, msgFmt
		}
		return res, msgFmt
	}

}

// 向指定链接推送消息
func (this *ReplyBody) SendMsgToUidDid(replyBodyProto *ProtoModel.ReplyBodyProto, uid string, Did string) {
	data, err := this.Marshal(replyBodyProto)
	if data == nil {
		// 格式化失败
		log.Println(err)
		return
	}
	(&Service{}).SendUidDid(ProtoModel.DataTypeProto_REPLY, data, uid, Did)
}

// 向指定链接推送消息
func (this *ReplyBody) SendMsgToConn(replyBodyProto *ProtoModel.ReplyBodyProto, connId string) {
	data, msgFmt := this.Marshal(replyBodyProto)
	if data == nil {
		// 格式化失败
		log.Println(msgFmt)
		return
	}
	//messageProto.String()
	this.SendNew(data, connId, msgFmt)

}

// 向所有登录用户广播消息

func (this *ReplyBody) SendMsgToUsers(replyBodyProto *ProtoModel.ReplyBodyProto, uids []string) {
	data, msgFmt := this.Marshal(replyBodyProto)
	if data == nil {
		// 格式化失败
		log.Println(msgFmt)
		return
	}
	this.SendUids(data, uids)
}

// 向所有登录用户广播消息
func (this *ReplyBody) SendMsgToAll(replyBodyProto *ProtoModel.ReplyBodyProto) {
	data, msgFmt := this.Marshal(replyBodyProto)
	if data == nil {
		// 格式化失败
		log.Println(msgFmt)
		return
	}
	this.SendAll(data)
}

// 向指定多个连接推送消息
func (this *ReplyBody) SendMsgToUsersConns(replyBodyProto *ProtoModel.ReplyBodyProto, conns []string) {
	for _, conn := range conns {
		this.SendMsgToConn(replyBodyProto, conn)
	}
}

// 遍历每条消息发送个所有人
func (this *ReplyBody) SendMsgArrToAll(msgArr []*ProtoModel.ReplyBodyProto) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToAll(replyBodyProto)
	}
}

// 遍历每条消息发送给接收人
func (this *ReplyBody) SendMsgArrToUserAll(msgArr []*ProtoModel.ReplyBodyProto) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToUser(replyBodyProto, replyBodyProto.Receiver)
	}
}

// 向指定登录用户发送多个消息
func (this *ReplyBody) SendMsgArrToUsers(msgArr []*ProtoModel.ReplyBodyProto, uids []string) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToUsers(replyBodyProto, uids)
	}
}

// 向指定登录用户发送多个消息
func (this *ReplyBody) SendMsgArrToUser(msgArr []*ProtoModel.ReplyBodyProto, uid string) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToUser(replyBodyProto, uid)
	}
}

// 向指定登录用户下某个设备发送消息
func (this *ReplyBody) SendMsgToFd(msg *ProtoModel.ReplyBodyProto, Cid string) {

	this.SendMsgToConn(msg, Cid)
}

// 向指定登录用户下某个设备发送消息
func (this *ReplyBody) SendMsgArrToFd(msgArr []*ProtoModel.ReplyBodyProto, Did string) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToFd(replyBodyProto, Did)
	}
}

// 向指定登录用户下某个设备发送消息
func (this *ReplyBody) SendMsgArrToUserFd(msgArr []*ProtoModel.ReplyBodyProto, uid string, Did string) {
	for _, replyBodyProto := range msgArr {
		this.SendMsgToUidDid(replyBodyProto, uid, Did)
	}
}

// 向指定房间发送消息 uid 升级成房间编号。
func (this *ReplyBody) SendMsgToRoom(msgArr []*ProtoModel.ReplyBodyProto, roomNo string) {

	userConnWs := global2.Ws_.GetRoomListVal(roomNo)
	if userConnWs != nil {
		for _, conn := range userConnWs {
			for _, messageProto := range msgArr {
				this.SendMsgToConn(messageProto, conn)
			}
		}
	}

	userConnTcp := global2.Tcp_.GetRoomListVal(roomNo)
	if userConnTcp != nil {
		for _, conn := range userConnTcp {
			for _, messageProto := range msgArr {
				this.SendMsgToConn(messageProto, conn)
			}
		}
	}
}

// 向指定登录用户发送消息
func (this *ReplyBody) SendMsgToUser(replyBodyProto *ProtoModel.ReplyBodyProto, uid string) {
	userConn := global2.Ws_.GetUserListVal(uid)
	for _, conn := range userConn {
		this.SendMsgToConn(replyBodyProto, conn)
	}
	userTcpConn := global2.Tcp_.GetUserListVal(uid)
	for _, conn := range userTcpConn {
		this.SendMsgToConn(replyBodyProto, conn)
	}
}
