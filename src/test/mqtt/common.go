package test

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var broker = "127.0.0.1"
var port = 1883
var userName1 = "user1"
var passwd1 = "123456"
var userName2 = "user2"
var passwd2 = "123456"

var topic = "root/topic"

func sub(client mqtt.Client, producer bool) {
	token := client.Subscribe(topic, 1, nil)
	token.Wait()
	if producer {
		fmt.Printf("Producer subscribed to topic %s", topic)
	} else {
		fmt.Printf("Consumer subscribed to topic %s", topic)
	}
}
