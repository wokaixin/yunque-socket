package start

import (
	"github.com/gorilla/mux"
	"log"
	"src/api/http/controller/admin"
	"src/api/http/controller/user"
	"src/config"
	"src/handler"
	service2 "src/service"
)

// 启动http服务
func HttpRouter() *mux.Router {
	//contextPath := config.V().GetString("server.http.servlet.context-path")
	http1 := mux.NewRouter()
	// 使用中间件 捕获异常
	//http1.Use(utils.Middleware)
	//http1 := http.NewServeMux()
	isOpen := config.V().GetBool("server.http.user-api.isOpen")
	if isOpen {
		contextPath := config.V().GetString("server.http.servlet.context-path")
		log.Println(contextPath)
		(&RouterGroup{RouterMod: RouterMod{Name: "user", Path: contextPath}}).
			//AddGroup((&RouterGroup{ RouterMod:RouterMod{Name: "user", Path: "/user"}}).
			//	//模拟登录 返回jwt token
			//	Add((&Router{ RouterMod:RouterMod{Path: "/login", Run: user.NewUserC().Login}})).
			//	// 模拟注册
			//	Add((&Router{RouterMod:RouterMod{Path: "/reg", Run: user.NewUserC().Reg}}))).
			AddGroup(
				//Reg(http1)
				////模拟登录 返回jwt token
				//(&Router{Path:  "/user/login", Run: user.NewUserC().Login}).Reg(http1)
				//// 模拟注册
				//(&Router{Path:  "/user/reg", Run: user.NewUserC().Reg}).Reg(http1)
				// 模拟获取用户信息 	//这行代码意思:http访问 Path 地址触发执行 Run 控制器，在Run被触发前按顺序执行OnBefore 里面的方法，before里面的方法调用req.next(req)是继续，否则是终止
				(&RouterGroup{RouterMod: RouterMod{Name: "user", Path: "/user", OnBefore: []func(*service2.Req){(&service2.Req{}).Auth}}}).
					// 获取用户信息
					Add((&Router{RouterMod: RouterMod{Path: "/getUserinfo", Run: user.NewUserC().GetUserinfo}})).
					// 获取用户列表
					Add((&Router{RouterMod: RouterMod{Path: "/getList", Run: user.NewUserC().GetList}})).
					// 模拟获取消息列表
					Add((&Router{RouterMod: RouterMod{Path: "/message/getList", Run: user.NewMessageC().GetList}})).
					// 模拟发送消息
					Add((&Router{RouterMod: RouterMod{Path: "/message/send", Run: user.NewMessageC().Send}})).
					//AddGroup(
					//下面是留给外部应用调用的接口 需要控制安全 防止外网访问
					//(&RouterGroup{RouterMod: RouterMod{Name: "user", Path: "/user"}}).
					// 模拟通过http请求绑定用户cid 和uid关系
					Add(&Router{RouterMod: RouterMod{Path: "/bindUser", Run: admin.NewBind().BindUser}}).
					// 通过http请求绑定用户uid绑定RoomNo 房间号关系
					Add(&Router{RouterMod: RouterMod{Path: "/bindRoom", Run: admin.NewBind().BindRoom}}).
					// 通过http请求 用户退出房间
					Add(&Router{RouterMod: RouterMod{Path: "/outRoom", Run: admin.NewBind().OutRoom}})).
			AddGroup(
				(&RouterGroup{RouterMod: RouterMod{Name: "message", Path: "/message", OnBefore: []func(*service2.Req){(&service2.Req{}).Auth}}}).
					// 模拟获取消息列表
					Add((&Router{RouterMod: RouterMod{Path: "/getList", Run: user.NewMessageC().GetList}})).
					// 模拟发送消息
					Add((&Router{RouterMod: RouterMod{Path: "/send", Run: user.NewMessageC().Send}}))).
			AddGroup(
				(&RouterGroup{RouterMod: RouterMod{Name: "admin", Path: "/admin"}}).
					AddGroup(
						(&RouterGroup{RouterMod: RouterMod{Name: "app", Path: "/app"}}).
							// 创建应用
							Add(&Router{RouterMod: RouterMod{Path: "/addApp", Run: admin.NewApp().AddApp}})).
					// 通过http请求获得房间下的所有用户uid
					Add(&Router{RouterMod: RouterMod{Path: "/getRoomAllUser", Run: admin.NewAdmin().GetRoomAllUser}}).

					// 通过http请求获得房间下的所有用户uid
					Add(&Router{RouterMod: RouterMod{Path: "/getAllRoom", Run: admin.NewAdmin().GetAllRoom}}).
					//查看在线人信息
					Add(&Router{RouterMod: RouterMod{Path: "/getOnlineUser", Run: admin.NewAdmin().GetOnlineUser}})).
			AddGroup(
				(&RouterGroup{RouterMod: RouterMod{Name: "send", Path: "/send"}}).
					//推送一条消息
					Add(&Router{RouterMod: RouterMod{Path: "/toUid", Run: admin.NewMessage().Send}}).
					//给多个用户推送 一条消息
					Add(&Router{RouterMod: RouterMod{Path: "/toUids", Run: admin.NewMessage().SendUids}}).
					//给用户的某个在线ID推一条消息
					Add(&Router{RouterMod: RouterMod{Path: "/toUidDid", Run: admin.NewMessage().SendUidDid}}).
					//推一组消息
					Add(&Router{RouterMod: RouterMod{Path: "/sendList", Run: admin.NewMessage().SendList}}).
					//推给房间内所有人
					Add(&Router{RouterMod: RouterMod{Path: "/toRoom", Run: admin.NewMessage().SendRoom}}).
					//推送消息给所有人
					Add(&Router{RouterMod: RouterMod{Path: "/all", Run: admin.NewMessage().SendAll}})).Reg(http1)
	}
	return http1
}

// 启动websocket服务
func WebSocketRouter() *mux.Router {
	contextPath := config.V().GetString("server.websocket.servlet.context-path")
	http1 := mux.NewRouter()
	(&RouterGroup{RouterMod: RouterMod{Name: "ws", Path: contextPath}}).
		Add((&Router{RouterMod: RouterMod{Path: "", Run: handler.NewWebSocketHandler().RequestHandler}})).
		//(&Router{Path: contextPath, Run: handler.NewWebSocketHandler().RequestHandler}).Reg(http1)

		Add(&Router{RouterMod: RouterMod{Path: "/{act}/{value}", Run: handler.NewWebSocketHandler().RequestHandler}}).Reg(http1)
	log.Println(contextPath)
	//Reg(&Router{Path: "/ws/{token}/{roomNo}", Run: service.NewWebSocketHandler().RequestHandler}))
	//监听websocket服务端口
	//log.Fatal(http.ListenAndServe(ip, http1))
	return http1
}

// 启动TCP服务
func TcpRouter() *handler.TcpHandler {
	log.Println("TcpStart\n")
	//http1 := http.NewServeMux()
	//Reg(&Router{Path: "/ws", Run: service.NewWebSocketHandler().RequestHandler}))
	//监听websocket服务端口
	//handler.NewTcpHandler().TcpHandler(ip, port)
	return handler.NewTcpHandler()
}
