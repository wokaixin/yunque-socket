package model

type RoomBindModel struct {
	//房间令牌
	Token string `json:"token"`
	//有效期
	Time string `json:"time"`
	//房间号
	RoomNo string `json:"roomNo"`
	//昵称
	Nickname string `json:"nickname"`
	//头像
	Avatar string `json:"avatar"`
	Uid    string `json:"uid"`
}
