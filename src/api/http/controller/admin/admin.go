package admin

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"src/global"
	"src/model"
	"src/service"
)

type Admin struct {
}

var Admins *Admin

func NewAdmin() *Admin {
	return &Admin{}
}

//
//// post 请求参数 test
//func Test(b *service.Req) {
//	fmt.Println(b.GetParams())
//	global.Ws_.Inc()
//	time.Now()
//	fmt.Println(time.Now())
//	var id int
//	for i := 0; i < 100000000; i++ {
//		id++
//	}
//	fmt.Println(time.Now())
//	fmt.Println(id)
//	//var ls  []int64
//	//ls=append(ls,(*global.Get()).Id)
//	fmt.Println(global.Ws_.Id)
//}

// 获取所有房间
func (*Admin) GetAllRoom(b *service.Req) {

	rooms := global.Ws_.RoomList

	if rooms == nil {
		b.Fail("房间不存在")
		return
	}

	b.Ok(rooms)
}

// 获取房间内的所有用户 RoomNo 必填
func (*Admin) GetRoomAllUser(b *service.Req) {
	var err error
	// 接收post参数
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		log.Println(err)
		b.Fail("读取body失败")
		return
	}
	//var reg model.LoginModelReg
	userBind := model.RoomBindModel{}
	err = json.Unmarshal(body, &userBind)
	if err != nil {
		b.Fail("json格式失败")
		return
	}

	if userBind.RoomNo == "" {
		b.Fail("RoomNo 不能为空")
		return
	}
	room := global.Ws_.RoomList[userBind.RoomNo]

	if room == nil {
		b.Fail("房间不存在")
		return
	}

	b.Ok(room)
}

// 查询在线用户信息
func (*Admin) GetOnlineUser(b *service.Req) {
	//已完成登录绑定用户
	userlist := global.Ws_.UserList
	var userlistInfos []map[string][]model.OnlineUserinfoModel
	var idx = 0
	for uid, cons := range userlist {
		userlistInfos = append(userlistInfos[:], make(map[string][]model.OnlineUserinfoModel))
		for _, conId := range cons {
			con := global.Ws_.ConnList[conId]
			userlistInfos[idx][uid] = append(userlistInfos[idx][uid][:], model.OnlineUserinfoModel{RoomNo: con.RoomNo, Uid: uid, Id: con.Id, Info: con.User, Times: con.Times})
			//userlistInfos[uid][con.Id].Uid = uid
			//userlistInfos[uid][con.Id].Id = con.Id
			//userlistInfos[uid][con.Id].ConnInfo = con.User
		}
		idx++
	}
	//所有链接
	connlists := global.Ws_.ConnList
	var connlistInfos []map[string]model.OnlineUserinfoModel
	var idx2 = 0
	for cid, con := range connlists {
		connlistInfos = append(connlistInfos[:], make(map[string]model.OnlineUserinfoModel))
		connlistInfos[idx2][cid] = model.OnlineUserinfoModel{Uid: con.Uid, RoomNo: con.RoomNo, Id: con.Id, Info: con.User, Times: con.Times}
		idx2++
	}
	data := make(map[string]interface{})
	data["authUser"] = &userlistInfos
	data["all"] = &connlistInfos
	b.Ok(data)
	//w.Write(jsonStr)
}
