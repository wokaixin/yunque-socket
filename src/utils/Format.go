package utils

import (
	"encoding/json"
	"google.golang.org/protobuf/proto"
	"log"
)

type Proto struct {
}
type Json struct {
}

func (*Proto) Marshal(m proto.Message) []byte {
	data, err := proto.Marshal(m)
	if err != nil {
		log.Println(err)
		return nil
	}
	return data
}
func (*Proto) Unmarshal(m []byte, res proto.Message) error {
	err := proto.Unmarshal(m, res)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}
func (*Json) Unmarshal(data []byte, v any) error {
	err := json.Unmarshal(data, v)
	if err != nil {
		log.Println(err)
		return err
	}
	return err
}
func (*Json) Marshal(v any) []byte {
	res, err := json.Marshal(v)
	if err != nil {
		log.Println(err)
		return nil
	}
	return res
}
