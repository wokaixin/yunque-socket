@echo off
:: #编译成 linux或win可执行文件set act=linux或者win
:: #执行方法 build.bat win

if "%~1"=="" (
set  act=linux
) else (
set  act=%~1
)

if %act%==linux (
set GOOS=linux
set GOARCH=amd64
go build -o ./bin/yunque  ./src/main.go
) ELSE (
SET GOOS=windows
go build -o ./bin/yunque.exe  ./src/main.go
)
echo %act%
pause