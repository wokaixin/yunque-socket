package model

type OnlineUserinfoModel struct {
	Id     string
	Uid    string
	Times  int64
	RoomNo string
	Info   ConnUserinfoModel
}
