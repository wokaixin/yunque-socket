package utils

import (
	"fmt"
)

type PkgBuf struct {
}

// 加密
func (PkgBuf) ByteEncrypt(num int64, len int) []byte {
	x := num
	byteData := make([]byte, len)
	for i := 0; i < len; i++ {
		byteData[i] = byte(x & 255) //进行与运算
		x = x >> 8                  //右移8位
	}
	return byteData
}

// 打包给客户端的数据
func (this PkgBuf) PkgData(protoData []byte, dataSize int64) []byte {
	pkgsize := dataSize + 10
	datapkg := make([]byte, pkgsize)
	//数据长度
	pByteLength := this.ByteEncrypt(pkgsize, 2)
	datapkg[0] = pByteLength[0]
	datapkg[1] = pByteLength[1]
	//网关类型
	pGateType := this.ByteEncrypt(1000, 2)
	datapkg[2] = pGateType[0]
	datapkg[3] = pGateType[1]
	//消息协议
	pProtoData := this.ByteEncrypt(2, 2)
	datapkg[4] = pProtoData[0]
	datapkg[5] = pProtoData[1]
	//UID
	pUID := this.ByteEncrypt(573232, 4)
	datapkg[6] = pUID[0]
	datapkg[7] = pUID[1]
	datapkg[8] = pUID[2]
	datapkg[9] = pUID[3]
	//数据实体
	for i, _ := range protoData {
		datapkg[i+10] = protoData[i]
	}
	fmt.Println(fmt.Sprintf("进行包装后的包头数据: [%v %v %v %v %v %v %v %v %v %v]", datapkg[0], datapkg[1], datapkg[2], datapkg[3], datapkg[4], datapkg[5], datapkg[6], datapkg[7], datapkg[8], datapkg[9]))
	fmt.Println(fmt.Sprintf("进行包装后的Protobuf实体数据: %v ", protoData))
	//fmt.Println(fmt.Sprintf("进行包装后的完整二进制: %v ",datapkg))
	return datapkg
}

//// 启动线程，不断发消息
//go func(){
//	var (err error)
//	//构造二进制数据
//	loginResponse := &LoginResponse{
//		Result: 0,
//		UserId: 573232,
//	}
//	data,_ := proto.Marshal(loginResponse)
//	fmt.Println("proto结构二进制数据:",data)
//
//	//返回给客户端的数据：{"length":14,"type":1000,"protocol":2,"uid":48944,"data":[16,176,254,34]} //请求登陆成功
//	pkg:=pkgData(data,4) //测试包装数据 4->protobuf data struct len
//	fmt.Println("返回给客户端的二进制数据:",pkg)
//
//	for{
//		//发送二进制数据
//		if err = conn.WriteMessage(pkg);err != nil{
//			return
//		}
//		time.Sleep(3*time.Second)
//	}
//}()
