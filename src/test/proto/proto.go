package test

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"google.golang.org/protobuf/proto"
	"os"
	"src/model"
	"src/model/ProtoModel"
)

func buf() {
	msgs := make(map[string]interface{})
	msgs["uid"] = "1"
	msg := model.MessageModel{Msg: msgs, Type: "2", Act: "3"}
	//binary.Write(&b, binary.LittleEndian, msg)
	b, err := json.Marshal(msg)
	//buf := bytes.NewBuffer(b)
	var act uint8 = 1
	buf := bytes.NewBuffer([]byte{act})
	buf.Write(b)
	c, err := buf.ReadByte()
	if err != nil {
		panic(err)
	}
	var message model.MessageModel
	err2 := json.Unmarshal(buf.Bytes(), &message)
	if err2 != nil {
		panic(err)
	}
	//Session session = JSONUtils.fromJson(redisMessage.getBody(), Session.class);
	fmt.Println(int(binary.BigEndian.Uint16([]byte{0, c})))
	fmt.Println(message)
}
func protoTest() {
	msg_test := &ProtoModel.SendBodyProto{
		Timestamp: 12354,
		Key:       "XXXX",
		Data:      map[string]string{},
	}

	//加密
	msgDataEncoding, err := proto.Marshal(msg_test)
	if err != nil {
		panic(err.Error())
		return
	}
	//解密
	msgEntity := &ProtoModel.SendBodyProto{}
	err = proto.Unmarshal(msgDataEncoding, msgEntity)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
		return
	}

	fmt.Printf("姓名：%s\n\n", msgEntity.Timestamp)
	fmt.Printf("年龄：%d\n\n", msgEntity.Key)
	fmt.Printf("国籍：%s\n\n", msgEntity.Data)
}

// 加密
func bufMessage() []byte {
	msg := &ProtoModel.TransportableProto{
		Body: []byte("xxxxxxxxx"),
		Type: []byte{2},
	}
	//加密
	msgDataEncoding, err := proto.Marshal(msg)
	if err != nil {
		panic(err.Error())
		return nil
	}
	return msgDataEncoding
}
func main() {
	bufmsg := bufMessage()
	tb := &ProtoModel.TransportableProto{}
	//解码
	err2 := proto.Unmarshal(bufmsg, tb)
	if err2 == nil {
		fmt.Println(tb)

		fmt.Println(ProtoModel.DataTypeProto(tb.Type[0]))
		fmt.Println(tb.Body)
		fmt.Printf("%T", tb)
	}

	//ProtoModel.DataType().UnmarshalJSON()
	//fmt.Println(ProtoModel.DataTypeProto_PING)
	//dataType := ProtoModel.DataTypeProto_SENT.Number()
	//
	//fmt.Println(dataType)
	//fmt.Println(dataType)
	//int32 转 int8
	//buf := make([]byte, 8)
	//binary.BigEndian.PutUint32(buf, uint32(dataType))
	//fmt.Println(int(buf))
	fmt.Println("\n=================================\n")
	fmt.Println([]byte{uint8(ProtoModel.DataTypeProto_SENT.Number())})
	fmt.Println("\n=================================\n")

}
