package global

import (
	"src/config"
)

type ConstantModel struct {
	RedisTokenPrefix string
}

func Constant() *ConstantModel {
	return &ConstantModel{
		//subscribe token 前缀
		RedisTokenPrefix: config.V().GetString("client.redis-token-prefix"),
	}
}
