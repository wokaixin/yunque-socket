package admin

import (
	"encoding/json"
	"io/ioutil"
	"log"
	global2 "src/global"
	"src/model"
	"src/model/ProtoModel"
	service2 "src/service"
	"src/utils"
	"time"
)

type Message struct {
}

var MessageS *Message

func NewMessage() *Message {
	return &Message{}
}

// post 请求参数 test
func (*Message) SendBody(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		b.Fail("参数有误")
		return
	}

	var data ProtoModel.SendBodyProto
	err = json.Unmarshal(body, &data)
	if err != nil {

		b.Fail("参数有误")
		return
	}

	data.Timestamp = time.Now().Unix()
	//Json结构返回
	service2.SendAll(&service2.ReplyBody{}, body)

	b.Ok("完成")

}

// post 请求参数 test
func (*Message) Send(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		b.Fail("参数有误")
		return
	}

	var data ProtoModel.MessageProto
	err = json.Unmarshal(body, &data)
	if err != nil {
		b.Fail("参数不正确")
		return
	}
	//
	//data.Id = global.Ws_.Id
	//global.Ws_.Inc()
	//time.Now().Format("2006-01-02 15:04:05")
	data.SendTime = time.Now().Unix()
	//Json结构返回
	if data.Receiver != "" {
		//fmt.Println("data.Msg.Id______", data.Id)
		userConnWs := global2.Ws_.GetUserListVal(data.Receiver)
		if userConnWs == nil || len(userConnWs) == 0 {
			b.Fail("用户不在线")
			return
		}
		(&service2.Message{}).SendMsgToUser(&data, data.Receiver)
		b.Ok("完成")
		return
	}

	b.Fail("参数有误")

}

// post 请求参数 test
func (*Message) SendList(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		b.Fail("参数有误")
		return
	}

	var datas []ProtoModel.MessageProto
	err = (&utils.Json{}).Unmarshal(body, &datas)
	//err = json.Unmarshal(body, &datas)
	if err != nil {
		b.Fail("参数有误")
		return
	}
	for i := 0; i < len(datas); i++ {
		datas[i].SendTime = time.Now().Unix()
		//Json结构返回
		if datas[i].Receiver != "" {
			//fmt.Println("data.Msg.Id______", data.Id)
			//seddata := (&utils.Proto{}).Marshal(&data)
			(&service2.Message{}).SendMsgToUser(&datas[i], datas[i].Receiver)
			// 并发太高会有失败
		}
	}
	b.Ok("完成")

}

// post 请求参数  向用户某个设备推送一条消息
func (*Message) SendUidDid(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		log.Println(err)
		b.Fail("参数有误")
		return
	}
	var mod model.MessageConnModel
	err = (&utils.Json{}).Unmarshal(body, &mod)
	//err = json.Unmarshal(body, &datas)
	if err != nil || mod.Uid == "" || mod.Did == "" {
		b.Fail("参数有误")
		return
	}

	//mod.Message.Id = global.Ws_.Id
	//global.Ws_.Inc()
	//time.Now().Format("2006-01-02 15:04:05")
	mod.Message.SendTime = time.Now().Unix()
	//Json结构返回

	//fmt.Println("data.Msg.Id______", mod.Message.Id)
	//seddata := (&utils.Proto{}).Marshal(&data)

	userConnWs := global2.Ws_.GetUserListVal(mod.Uid)
	if userConnWs == nil || len(userConnWs) == 0 {
		b.Fail("用户不在线")
		return
	}
	//向所有已登录绑定的用户发消息
	for _, connid := range userConnWs {
		userConn := global2.Ws_.GetConnListVal(connid)
		if userConn.Did == mod.Did {
			(&service2.Message{}).SendMsgToUidDid(&mod.Message, mod.Uid, mod.Did)
			b.Ok("完成")
			return
		}
	}
	// 并发太高会有失败

	b.Fail("设备不在线")

}

// post 请求参数  向多个用户推送一条消息
func (*Message) SendUids(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		log.Println(err)
		b.Fail("参数有误")
		return
	}
	var mod model.MessageUsersModel
	err = (&utils.Json{}).Unmarshal(body, &mod)
	//err = json.Unmarshal(body, &datas)
	if err != nil || mod.Uids == nil || len(mod.Uids) < 1 {
		b.Fail("参数有误")
		return
	}

	//mod.Message.Id = global.Ws_.Id
	//global.Ws_.Inc()
	//time.Now().Format("2006-01-02 15:04:05")
	mod.Message.SendTime = time.Now().Unix()
	//Json结构返回
	//if mod.Message.Receiver != "" {
	//fmt.Println("data.Msg.Id______", mod.Message.Id)
	//seddata := (&utils.Proto{}).Marshal(&data)
	(&service2.Message{}).SendMsgToUsers(&mod.Message, mod.Uids)
	// 并发太高会有失败
	//}

	b.Ok("完成")

}

// post 请求参数  向房间里推送一条消息
func (*Message) SendRoom(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		log.Println(err)
		b.Fail("参数有误")
		return
	}
	var mod model.MessageRoomModel
	err = (&utils.Json{}).Unmarshal(body, &mod)
	//err = json.Unmarshal(body, &datas)
	if err != nil || mod.RoomNo == "" || len(mod.RoomNo) < 1 {

		b.Fail("参数有误")
		return
	}

	(&service2.Message{}).SendMsgToRoom(&mod.Message, mod.RoomNo)
	b.Ok("完成")

}

// 向所有链接广播消息
func (*Message) SendAll(b *service2.Req) {
	body, err := ioutil.ReadAll(b.R.Body)
	if err != nil {
		log.Println(err)
		b.Fail("参数有误")
		return
	}
	var data ProtoModel.MessageProto
	err = json.Unmarshal(body, &data)
	if err != nil {
		b.Fail("参数有误")
		return
	}
	//data.Id = global.Ws_.Id
	//global.Ws_.Inc()
	data.SendTime = time.Now().Unix()
	////Json结构返回

	// 并发太高会有失败
	//logic.SendMsgToAllConn(string(json))
	(&service2.Message{}).SendMsgToAll(&data)

	b.Ok(&data)
}
