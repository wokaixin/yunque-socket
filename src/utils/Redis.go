package utils

import (
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
)

type RedisClient struct {
	Name    string
	Client  *redis.Client
	Options *redis.Options
}

func Conf() *redis.Options {
	return &redis.Options{
		Addr:     viper.GetString("db.redis.conf.ip") + ":" + viper.GetString("db.redis.conf.port"),
		Password: viper.GetString("db.redis.conf.password"), // 如果没有设置密码，则为空字符串
		DB:       viper.GetInt("db.redis.conf.idx"),         // 使用默认数据库

	}
}
func NewRedisClient(name string, conf *redis.Options) *RedisClient {
	if conf == nil {
		conf = Conf()
	}
	client := redis.NewClient(conf)
	return &RedisClient{Name: name, Client: client, Options: conf}
}
