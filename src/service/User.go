package service

import (
	"errors"
	"log"
	"src/mapper"
	"src/utils"
)

type User struct {
	userinfo *mapper.User
	userCons []string
}

var UserI *User

func (this *User) AddUser(user *mapper.User) error {
	db := utils.NewDbClient("db", nil)
	//user := new(mapper.User)
	_, err := db.Client.Insert(user)
	////sqlStr := "SELECT userid as uid,name as nickname,avatar from user WHERE token = ?"
	////row := mysql.Client.QueryRow(sqlStr, token)
	//loginAuthType := config.V().GetString("client.login-auth-type")
	//sql_2_1 := config.V().GetString("client." + loginAuthType + ".add-user-sql")
	//if sql_2_1 == "" {
	//	demosql := "INSERT INTO `user` (phone,password,name,avatar,token)  VALUES(?,?,?,?,?)"
	//	log.Println("配置文件未配置数据库登录授权节点, 节点位置:%v\n", "client."+loginAuthType+".auth-sql"+";参考值+"+demosql)
	//	return errors.New("配置文件未配置数据库登录授权节点")
	//}
	//_, err := db.Client.Exec(sql_2_1, username, password, nickname, avatar, token)
	////然后使用Scan()方法给对应类型变量赋值，以便取出结果,注意传入的是指针
	////err := row.Scan(&userinfo.Uid, &userinfo.Nickname, &userinfo.Avatar)
	if err != nil {
		log.Println("创建用户失败, err:%v\n", err.Error())
		return errors.New(err.Error())
	}
	return nil
}
func (this *User) UpdateUser(user *mapper.User) error {
	db := utils.NewDbClient("db", nil)

	_, err := db.Client.Update(user)
	////sqlStr := "SELECT userid as uid,name as nickname,avatar from user WHERE token = ?"
	////row := mysql.Client.QueryRow(sqlStr, token)
	//loginAuthType := config.V().GetString("client.login-auth-type")
	//sql_2_1 := config.V().GetString("client." + loginAuthType + ".update-user-sql")
	//if sql_2_1 == "" {
	//	demosql := "UPDATE `user` SET password=?,name=?,avatar=?,token=? WHERE `userid` = ?"
	//	log.Println("配置文件未配置数据库登录授权节点, 节点位置:%v\n", "client."+loginAuthType+".auth-sql"+";参考值+"+demosql)
	//	return errors.New("配置文件未配置数据库登录授权节点")
	//}
	//_, err := db.Client.Exec(sql_2_1, password, nickname, avatar, token, uid)
	//然后使用Scan()方法给对应类型变量赋值，以便取出结果,注意传入的是指针
	//err := row.Scan(&userinfo.Uid, &userinfo.Nickname, &userinfo.Avatar)
	if err != nil {
		log.Println("更新用户数据失败, err:%v\n", err.Error())
		return err
	}
	return nil
}
func (this *User) GetList(user *mapper.User, start int, num int) ([]mapper.User, error) {
	db := utils.NewDbClient("db", nil)
	var users = make([]mapper.User, 0)
	var err error
	if user == nil {
		err = db.Client.Limit(num, start).Find(&users)
	} else {
		err = db.Client.Limit(num, start).Find(&users, user)
	}
	if err != nil {
		return nil, err
	}
	return users, nil
}
func (this *User) GetUser(user *mapper.User) (*mapper.User, error) {
	db := utils.NewDbClient("db", nil)

	has, err := db.Client.Alias("u").Get(user)
	////sqlStr := "SELECT userid as uid,name as nickname,avatar from user WHERE token = ?"
	////row := mysql.Client.QueryRow(sqlStr, token)
	//loginAuthType := config.V().GetString("client.login-auth-type")
	//sql_2_1 := config.V().GetString("client." + loginAuthType + ".get-user-sql")
	//if sql_2_1 == "" {
	//	demosql := "select userid as uid, avatar ,password,name as nickname,phone as username from `user` where phone=? or userid=?"
	//	log.Println("配置文件未配置数据库登录授权节点, 节点位置:%v\n", "client."+loginAuthType+".auth-sql"+";参考值+"+demosql)
	//	return nil, errors.New("配置文件未配置数据库登录授权节点")
	//}
	//has, err := db.Client.SQL(sql_2_1, username, uid).Get(&userinfo)
	//
	////然后使用Scan()方法给对应类型变量赋值，以便取出结果,注意传入的是指针
	////err := row.Scan(&userinfo.Uid, &userinfo.Nickname, &userinfo.Avatar)
	//if err != nil {
	//	log.Println("获取mysql用户数据错误, err:%v\n", err.Error())
	//	return nil, err
	//}
	if has {
		return user, nil
	}
	return nil, err
}
