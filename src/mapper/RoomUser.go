package mapper

// 房间数据
type RoomUser struct {
	Id     string `json:"id"  description:"ID唯一"`
	Uid    string `json:"uid"  description:"用户UID"`
	RoomId string `json:"roomId"  description:"房间id"`
	Avatar string `json:"avatar" description:"头像"`
	//昵称
	Nickname   string `json:"nickname" description:"昵称"`
	Username   string `json:"username"  description:"用户帐户唯一"`
	Title      string `json:"title"  description:"房间名称"`
	Times      int    `json:"times" description:"在线时长"`
	CreateTime string `json:"createTime" description:"创建时间"`
	UpdateTime string `json:"updateTime" description:"更新时间"`
}
