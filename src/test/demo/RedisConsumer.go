package demo

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	"src/model"
)

func RedisConsumer(channel string) {

	client := redis.NewClient(&redis.Options{
		Addr:     viper.GetString("receive.subscribe.conf.ip") + ":" + viper.GetString("receive.subscribe.conf.port"),
		Password: viper.GetString("receive.subscribe.conf.password"), // 如果没有设置密码，则为空字符串
		DB:       viper.GetInt("receive.subscribe.conf.db"),          // 使用默认数据库
	})
	defer client.Close()
	//now := time.Now()
	// fmt.Println(now.Unix()) // 1565084298 秒fmt.Println(now.UnixNano()) // 1565084298178502600 纳秒fmt.Println(now.UnixNano() / 1e6) // 1565084298178 毫秒
	fmt.Println("end【0 】——————————开始接收订阅消息—————————!")
	sub := client.Subscribe(channel)
	//// 取消订阅
	//submodel.Unsubscribe("channel1")
	// submodel.Channel() 返回go channel，可以循环读取redis服务器发过来的消息
	//var b = 1
	for msg := range sub.Channel() {
		var result model.MessageModel
		err2 := json.Unmarshal([]byte(msg.Payload), &result)
		if err2 != nil {
			return
		}
		fmt.Println("result:", result)
		SendRouterMq(result)
		//if b%50000 == 0 {
		//	//fmt.Println("run【", b, "】——————————接收订阅消息中—————————!")
		//	fmt.Println("耗时秒", (time.Now()).Unix()-now.Unix(), "耗时毫秒", ((time.Now()).UnixNano()/1e6)-(now.UnixNano()/1e6))
		//	fmt.Println("run【", b, "】订阅：", m)
		//}
	}

}
