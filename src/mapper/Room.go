package mapper

// 房间数据
type Room struct {
	Id     string `json:"id"  description:"ID唯一"`
	Uid    string `json:"uid"  description:"用户UID"`
	Avatar string `json:"avatar" description:"头像"`
	//昵称
	Nickname   string `json:"nickname" description:"昵称"`
	Username   string `json:"username"  description:"用户帐户唯一"`
	Title      string `json:"title"  description:"房间名称"`
	Token      string `json:"token" description:"token令牌"`
	CreateTime string `json:"createTime" description:"创建时间"`
	UpdateTime string `json:"updateTime" description:"更新时间"`
}
