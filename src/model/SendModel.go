package model

type SendModel struct {
	Key  string      `json:"key"`
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}
