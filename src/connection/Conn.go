package connection

type Conn interface {
	GetConn()
	UpdateTime()
	ReadMess() (data []byte, err error)
	WriteMes(data []byte) (err error)
	Close()
	ReadLoop()
	WriteLoop()
}
