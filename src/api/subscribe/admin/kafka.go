package admin

import (
	"github.com/IBM/sarama"
	"log"
	"src/handler"
)

type Kafka struct {
}

var KafkaS *Kafka

func NewKafka() *Kafka {
	return &Kafka{}
}

var idxk int64

// 接收订阅消息回调函数
func (*Kafka) SubChat(pc *sarama.PartitionConsumer, v *sarama.ConsumerMessage) {
	idxk++
	log.Println(idxk, "Kafka 订阅接收到消息")
	handler.NewSubHandler().SubHandler(v.Topic, v.Value)
}
