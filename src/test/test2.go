package test

import (
	"encoding/json"
	"fmt"
	"src/model"
	"src/model/ProtoModel"
	"time"
)

func js(b []byte, e error) {

}
func run(ch chan int, c int) {
	var msg = "{\"act\":\"toConn\",\"type\":\"auth\",\"msg\":{\"uid\":\"5\",\"msg\":\"嗯嗯 很好吗\",\"touid\":\"222\"}}"
	var id int
	for i := 0; i < 100000; i++ {
		var result model.MessageModel
		err2 := json.Unmarshal([]byte(msg), &result)
		if err2 != nil {
			break
		}
		//fmt.Println("result:", result)
		//switch result.Type {
		//case "chat":
		//	// 聊天
		//	//将map数据转化为JSON格式字符串
		//	jsonBytes, err3 := json.Marshal(result.Msg)
		//	if err3 != nil {
		//		fmt.Println(" JSON 转换失败 Error:", err3)
		//	}
		//	js(jsonBytes, err3)
		//	switch result.Act {
		//	}
		//}
		id++
	}
	//fmt.Println(id)
	//fmt.Println((time.Now()).Unix())
	//fmt.Println((time.Now()).UnixNano() / 1e6)
	ch <- c
	return
}

func main() {
	messageProto := ProtoModel.MessageProto{Id: 2, Title: "XXXXFDF"}
	//print(protojson.Marshal(messageProto))
	res, _ := json.Marshal(messageProto)

	fmt.Println("data.Unmarshal:", string(res))
	return
	ch := make(chan int)
	//var wg sync.WaitGroup
	now := time.Now()
	// fmt.Println(now.Unix()) // 1565084298 秒fmt.Println(now.UnixNano()) // 1565084298178502600 纳秒fmt.Println(now.UnixNano() / 1e6) // 1565084298178 毫秒
	fmt.Println("end【0 】——————————开始—————————!")
	fmt.Println(now.UnixNano() / 1e6)
	fmt.Println(now.Unix())
	for i := 0; i < 20; i++ {
		//	//wg.Add(1)
		go run(ch, i)
		//
	}
	//fmt.Println(<-ch)
	//wg.Wait()

	for {
		select {
		case i := <-ch:
			//fmt.Println(i)
			fmt.Println("耗时秒", (time.Now()).Unix()-now.Unix())
			fmt.Println("耗时毫秒", ((time.Now()).UnixNano()/1e6)-(now.UnixNano()/1e6))
			fmt.Println("end【", i, "】——————————有数据—————————!")
		default:
			time.Sleep(time.Second)
			//fmt.Println("end【 】——————————完成—————————!")
		}
	}
	//for ret := range ch {
	//	fmt.Println(len(ch))
	//	fmt.Println(ret)
	//}

	//fmt.Println("end————————————————————————————————!")
}
