package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	config2 "src/config"
	"src/start"
	"syscall"
)

// 热重载相关
type Config struct {
	Message string
}

var conf = &Config{Message: "Before hot reload"}

func multiSignalHandler(signal os.Signal) {
	switch signal {
	case syscall.SIGHUP:
		log.Println("Signal:", signal.String())
		log.Println("After hot reload")
		conf.Message = "Hot reload has been finished."
	case syscall.SIGINT:
		log.Println("Signal:", signal.String())
		log.Println("Interrupt by Ctrl+C")
		os.Exit(0)
	case syscall.SIGTERM:
		log.Println("Signal:", signal.String())
		log.Println("Process is killed.")
		os.Exit(0)
	default:
		log.Println("Unhandled/unknown signal")
	}
}
func main() {
	flag.StringVar(&config2.Env.Active, "active", "", "运行环境") // 定义默认环境
	flag.Parse()
	log.Println("程序启动") // 解析标志
	log.Println("active:" + config2.Env.Active)

	start.Start()
	log.Println("格式化类型" + config2.V().GetString("client.msg-fmt"))

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
	for {
		multiSignalHandler(<-sigCh)
	}
	//update_on_line_time_fang_user := config.V().GetBool("heartbeat.is-update-on-line-time-fang-user")
	//sql := config.V().GetString("heartbeat.update-on-line-time-fang-user-sql")
	//log.Println(update_on_line_time_fang_user, sql)
	//log.Println(config.V().GetString("auth.ipBlacklist.test"))
	// 监听文件变化，并自动重启服务
	select {}
}
