package model

type UserBindProto struct {
	//ws链接id
	Cid string
	//房间号
	RoomNo string
	//用户uid
	Uid         string
	Token       string
	Channel     string
	AppVersion  string
	OsVersion   string
	PackageName string
	DeviceId    string
	DeviceName  string
	Language    string
}
