package global

import (
	"src/connection"
)

type Tcp struct {
	ConnList map[string]*connection.Tcp
	ConnInfo
}

var Tcp_ *Tcp

func TcpInit() {
	Tcp_ = NewTcp()
}
func NewTcp() *Tcp {
	//Tcp_:=NewConnInfo();
	Tcp_ := &Tcp{make(map[string]*connection.Tcp), *NewConnInfo()}
	//ws在线链接
	Tcp_.ConnList = make(map[string]*connection.Tcp)
	return Tcp_
}

func (this *Tcp) Get() *Tcp { return Tcp_ }

// 唯一id自增
func (this *Tcp) Inc() *Tcp {
	Tcp_.Id++
	return Tcp_
}
func (this *Tcp) GetConnList() map[string]*connection.Tcp {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	return this.ConnList
}
func (this *Tcp) GetConnListVal(key string) *connection.Tcp {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	return this.ConnList[key]
}

func (this *Tcp) UpdateConnList(key string, value *connection.Tcp) {
	this.mutex.Lock()
	defer this.mutex.Unlock()
	this.ConnList[key] = value
}

func (this *Tcp) DelConnList(key string) {
	defer this.mutex.Unlock()
	this.mutex.Lock()
	if this.ConnList[key] != nil {
		delete(this.ConnList, key)
	}
}
