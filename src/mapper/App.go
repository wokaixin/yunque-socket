package mapper

// Id     int64  `json:"id"  description:"appID唯一" xorm:"varchar(32) id pk <- unique comment('姓名')"`
// 用户数据
type App struct {
	Id     int64  `json:"id"  description:"appID唯一"`
	Logo   string `json:"logo" description:"头像"`
	Mobile string `json:"mobile"  description:"手机号" validate:"required,min=11,max=15"`
	Email  string `json:"email"  description:"邮箱号" validate:"required,email"`
	//昵称
	Name       string `json:"name" description:"应用名称" validate:"required,min=2,max=20"`
	Username   string `json:"username"  description:"用户帐户唯一"`
	Password   string `json:"password"  description:"用户密码"`
	Token      string `json:"token" description:"token令牌"`
	AppKey     string `json:"appKey" description:"app_key" validate:"required,min=16,max=32"`
	AppSecret  string `json:"appSecret" description:"app_secret" validate:"required,min=32,max=128"`
	PrivateKey string `json:"privateKey" description:"私有key"`
	PublicKey  string `json:"publicKey" description:"公有key"`
	Status     int    `json:"status" description:"状态码"`
	AuthType   string `json:"authType" description:"应用鉴权类型"`
	Type       int    `json:"type" description:"应用类型"`
	Remark     string `json:"remark" description:"备注"`
	CreateTime string `json:"createTime" description:"创建时间" xorm:"<- comment('创建时间')"`
	UpdateTime string `json:"updateTime" description:"更新时间" xorm:"<- comment('创建时间')"`
	Deleted    byte   `json:"deleted" description:"是否删除"`
}

//func (u App) TableName() string {
//	return "app"
//}
