/**
* @author: yichen
* @Date: 2023/9/14 0014
* @Description: 向多用户发消息模版

* @return:
    **/

package model

import "src/model/ProtoModel"

type MessageUsersModel struct {
	Uids    []string                `json:"uids"`
	Message ProtoModel.MessageProto `json:"message"`
}
