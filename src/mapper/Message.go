package mapper

import (
	"time"
)

// 用户数据
type Message struct {
	Id              int64    `json:"id"  description:"ID唯一" xorm:"id pk autoincr <- unique comment('ID')"`
	Sender          string   `json:"sender"  description:"发送人UID" xorm:"varchar(32)   comment('发送人UID')"`
	Receiver        string   `json:"receiver"  description:"接收人UID" xorm:"varchar(32)   comment('发送人UID')"`
	Action          string   `json:"action" description:"消息动作"`
	Content         string   `json:"content" description:"内容"`
	Extra           string   `json:"extra" description:"拓展字段"`
	Type            string   `json:"type" description:"消息类型"`
	ReadState       int      `json:"readState" description:"读取状态"  xorm:"comment('阅读状态-1,已撤回,0未接受，1已接收,2已读')"`
	ReadTime        JsonTime `json:"readTime" description:"读取时间" xorm:"DATETIME  default null  comment('读取时间')"`
	GetTime         JsonTime `json:"getTime" description:"接收时间" xorm:"DATETIME comment('接收时间')"`
	SenderDelTime   JsonTime `json:"senderDelTime" description:"发送人删除时间" xorm:"DATETIME comment('发送人删除时间')"`
	ReceiverDelTime JsonTime `json:"receiverDelTime" description:"接收人删除时间" xorm:"DATETIME comment('接收人删除时间')"`
	WithdrawTime    JsonTime `json:"withdrawTime" description:"撤回时间" xorm:"DATETIME comment('撤回时间')"`
	SendTime        JsonTime `json:"sendTime" description:"发送时间" xorm:"DATETIME  comment('创建时间')"`
	UpdateTime      JsonTime `json:"updateTime" description:"更新时间" xorm:"created DATETIME  comment('更新时间')"`
	DeleteTime      JsonTime `json:"deleteTime" description:"删除时间" xorm:"DATETIME  comment('删除时间')"`
	IsDel           int      `json:"isDel" description:"是否已删除"  xorm:"comment('是否已删除')"`
}
type JsonTime time.Time

func (j JsonTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + time.Time(j).Format("2006-01-02 15:04:05") + `"`), nil
}
