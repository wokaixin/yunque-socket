package model

type ReplyModel struct {
	Key  string      `json:"key"`
	Code int         `json:"code"`
	Data interface{} `json:"data"`
	Msg  string      `json:"msg"`
}
