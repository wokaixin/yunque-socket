package test

import (
	"bytes"
	"fmt"
	"google.golang.org/protobuf/proto"
	"log"
	"net"
	"reflect"
	"src/connection"
	"src/model"
	"src/model/ProtoModel"
	"strconv"
	"sync"
	"time"
)

// 发消息
func sendMessage(i int) []byte {
	var idx int64 = int64(i)
	msgm := ProtoModel.MessageProto{Id: idx}
	msgm.Receiver = "1074"
	msgm.SendTime = time.Now().Unix()
	msgm.Content = map[string]string{"abc": "bcd"}
	//msg := ProtoModel.MessagesPubProto{}
	//msg.Msg = append(msg.Msg, &msgm)
	//msg.Sid = "beijing1"
	//msg.Uid = "1076"
	marshal, err := proto.Marshal(&msgm)
	if err != nil {
		return nil
	}
	buf := bytes.Buffer{}
	//buf.WriteByte(byte(ProtoModel.SendActProto_SENDALL.Number()))
	buf.WriteByte(byte(ProtoModel.DataTypeProto_MESSAGE.Number()))
	buf.Write(marshal)
	return buf.Bytes()
}

// 结构体转 map
func StructToMap(obj interface{}) map[string]string {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)
	var data = make(map[string]string)
	for i := 0; i < t.NumField(); i++ {
		data[t.Field(i).Name] = v.Field(i).String()
	}
	return data
}

// 请求结构图数据包
func sendBody(i int) []byte {
	var idx int64 = int64(i)
	msgm := &ProtoModel.SendBodyProto{}
	msgm.Key = "client_bind"
	//msgm.Data = time.Now().Unix()
	client := model.ClientBindProto{}
	client.Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjE2LCJTY29wZSI6ODg4OCwiVU5hbWUiOiJhZG1pbjEwIiwiZXhwIjoxNjk1ODYxOTY2LCJpc3MiOiJqZXJyeSIsIlRva2VuIjoiIn0.2LZwErvEmuMx7UrobT6fJimM-Nx2vDMu0dAXDI7Nob8"
	client.DeviceId = ""
	client.Language = ""
	client.DeviceName = ""
	client.OsVersion = ""
	client.AppVersion = ""
	client.Channel = string(idx)
	client.PackageName = ""

	clidata := StructToMap(client)
	msgm.Data = clidata
	//msg := ProtoModel.MessagesPubProto{}
	//msg.Msg = append(msg.Msg, &msgm)
	//msg.Sid = "beijing1"
	//msg.Uid = "1076"
	marshal, err := proto.Marshal(msgm)
	if err != nil {
		return nil
	}
	buf := bytes.Buffer{}
	//buf.WriteByte(byte(ProtoModel.SendActProto_SENDALL.Number()))
	buf.WriteByte(byte(ProtoModel.DataTypeProto_SENT.Number()))
	buf.Write(marshal)
	//send(buf.Bytes(), ipaddr)
	return buf.Bytes()
}
func main() {
	var ip = "127.0.0.1"
	var port = 9101
	var addr = ip + ":" + strconv.Itoa(port)
	//启动服务端
	go createServer(ip, port)

	time.Sleep(1 * time.Second)

	byts2 := sendMessage(1)
	//go func() {
	// 1. 建立客户端
	client := getClient(addr)
	conn, _ := (&connection.Tcp{}).GetConn(client)
	go onMessage(conn)
	//每秒发一条消息
	//go func() {
	for {
		err := conn.WriteMes(byts2)
		if err != nil {
			log.Println(err)
		}
		time.Sleep(5 * time.Second)
	}
	time.Sleep(2 * time.Second)
	select {}
	//}

}

// 创建一个服务端tcp监听服务
func createServer(ip string, port int) {
	var sport string = strconv.Itoa(port)
	//
	// 1. 建立tcp连接监听通道
	listen, err := net.Listen("tcp", ":"+sport)
	if err != nil {
		panic(err)
	}
	// 3. 关闭监听通道
	defer listen.Close()
	fmt.Println("server is Listening")
	// 2. 进行通道监听
	conn, err := listen.Accept()
	if err != nil {
		panic(err)
	}
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		for {
			//循环写
			wTcpConn := ""
			fmt.Scanln(&wTcpConn) //阻塞
			if wTcpConn == "over" {
				conn.Write([]byte(wTcpConn))
				break
			}
			conn.Write([]byte(wTcpConn))
		}

	}()
	tcpCon, _ := (&connection.Tcp{}).GetConn(&conn)
	go func() {
		defer wg.Done()
		for {
			handleRequest(tcpCon)
		}

	}()
	wg.Wait()
	defer conn.Close()
}

var idx int

// 服务端接收到消息
func handleRequest(conn *connection.Tcp) {

	//fmt.Println(conn.ReadMess())
	msgbytes, err := conn.ReadMess()
	if err != nil {
		log.Println(err)
	}
	//log.Println(msgbytes)
	//fmt.Println(string(msgbytes))
	buffer1 := bytes.NewBuffer(msgbytes)
	//更新心跳检测时间
	conn.UpdateTime()
	unreadBytes, _ := buffer1.ReadByte()
	dataType := ProtoModel.DataTypeProto(unreadBytes)
	if dataType == ProtoModel.DataTypeProto_MESSAGE {
		msgm1 := ProtoModel.MessageProto{}
		bytess := buffer1.Bytes()
		proto.Unmarshal(bytess, &msgm1)
		fmt.Println(dataType)
		fmt.Println(msgm1.String())
	}
	if dataType == ProtoModel.DataTypeProto_SENT {
		msgm2 := ProtoModel.SendBodyProto{}
		bytess := buffer1.Bytes()
		err := proto.Unmarshal(bytess, &msgm2)
		if err != nil {
			return
		}
		fmt.Println(dataType)
		fmt.Println(msgm2.String())
	}
	idx++
	//发包给 新链接的客户端
	err = conn.WriteMes([]byte("服务端发给客户端的" + strconv.Itoa(idx)))
	if err != nil {
		return
	}

}
func getClient(addr string) *net.Conn {
	// 1. 建立访问通道
	client, err := net.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}
	return &client
}

// 客户端发消息
func clientSend(msg []byte, conn net.Conn) {
	_, err := (conn).Write(msg)
	if err != nil {
		return
	}
}

// 客户端 监听消息 和服务端逻辑差不多一样的
func onMessage(conn *connection.Tcp) {

	for {
		msg, err := conn.ReadMess()
		if err != nil {
			continue
		}
		fmt.Println("client send info to server si : ", string(msg))
	}
}

// 客户端 发消息 和服务端逻辑差不多一样的
func send(msg []byte, addr string) {
	// 1. 建立访问通道
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	//_, err = conn.Write([]byte(msg))

	_, err = conn.Write(msg)
	if err != nil {
		return
	}

}
