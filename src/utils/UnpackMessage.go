package utils

import (
	"bytes"
	"fmt"
	"src/model/ProtoModel"
)

type Message struct {
}

var MessageF *Message

// 订阅消息 格式解包
func (this *Message) UnpackMessage(byte2 []byte) (ProtoModel.SendActProto, ProtoModel.SendTypeProto, *bytes.Buffer, error) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err) // 这里的err其实就是panic传入的内容，55
		}
	}()
	buffers := bytes.NewBuffer(byte2)

	tp, err := buffers.ReadByte()
	if err != nil {

		return 0, 0, nil, err
	}
	dtype, err := buffers.ReadByte()
	if err != nil {

		return 0, 0, nil, err
	}
	dataType := ProtoModel.SendTypeProto(dtype)
	actType := ProtoModel.SendActProto(tp)

	return actType, dataType, buffers, nil
}
