package tcp

import (
	"google.golang.org/protobuf/proto"
	"log"
	global2 "src/global"
	"src/model/ProtoModel"
	"src/service"
)

type TcpMessage struct {
}

func GetTcpMessage() *TcpMessage {
	return &TcpMessage{}
}

// 向指定链接推送消息
func (this *TcpMessage) SendAll(data []byte) {
	(&service.Service{}).SendAll(ProtoModel.DataTypeProto_MESSAGE, data)
}

// 向指定链接推送消息
func (this *TcpMessage) Send(data []byte, connId string) {
	(&service.Service{}).Send(ProtoModel.DataTypeProto_MESSAGE, data, connId)
}

// 向指定链接推送消息
func (this *TcpMessage) SendUid(data []byte, uid string) {
	(&service.Service{}).SendUid(ProtoModel.DataTypeProto_MESSAGE, data, uid)
}

// 向指定链接推送消息
func (this *TcpMessage) SendUids(data []byte, uids []string) {
	(&service.Service{}).SendUids(ProtoModel.DataTypeProto_MESSAGE, data, uids)
}

// 向指定链接推送消息
func (this *TcpMessage) SendUidDid(data []byte, uid string, Did string) {
	(&service.Service{}).SendUidDid(ProtoModel.DataTypeProto_MESSAGE, data, uid, Did)
}

// 向指定链接推送消息
func (this *TcpMessage) SendMsgToUidDid(messageProto *ProtoModel.MessageProto, uid string, Did string) {
	data, err := proto.Marshal(messageProto)
	if err != nil {
		// 格式化失败
		log.Println(err)
		return
	}
	(&service.Service{}).SendUidDid(ProtoModel.DataTypeProto_MESSAGE, data, uid, Did)
}

// 向指定链接推送消息
func (this *TcpMessage) SendMsgToConn(messageProto *ProtoModel.MessageProto, connId string) {

	data, err := proto.Marshal(messageProto)
	if err != nil {
		// 格式化失败
		log.Println(err)
		return
	}
	this.Send(data, connId)
}

// 向所有登录用户广播消息

func (this *TcpMessage) SendMsgToUsers(messageProto *ProtoModel.MessageProto, uids []string) {
	data, err := proto.Marshal(messageProto)
	if err != nil {
		// 格式化失败
		log.Println(err)
		return
	}
	this.SendUids(data, uids)
}

// 向所有登录用户广播消息
func (this *TcpMessage) SendMsgToAll(messageProto *ProtoModel.MessageProto) {
	data, _ := proto.Marshal(messageProto)
	this.SendAll(data)
}

// 向指定多个连接推送消息
func (this *TcpMessage) SendMsgToUsersConns(messageProto *ProtoModel.MessageProto, conns []string) {
	for _, conn := range conns {
		this.SendMsgToConn(messageProto, conn)
	}
}

// 遍历每条消息发送个所有人
func (this *TcpMessage) SendMsgArrToAll(msgArr []*ProtoModel.MessageProto) {
	for _, messageProto := range msgArr {
		this.SendMsgToAll(messageProto)
	}
}

// 遍历每条消息发送给接收人
func (this *TcpMessage) SendMsgArrToUserAll(msgArr []*ProtoModel.MessageProto) {
	for _, messageProto := range msgArr {
		this.SendMsgToUser(messageProto, messageProto.Receiver)
	}
}

// 向指定登录用户发送多个消息
func (this *TcpMessage) SendMsgArrToUsers(msgArr []*ProtoModel.MessageProto, uids []string) {
	for _, messageProto := range msgArr {
		this.SendMsgToUsers(messageProto, uids)
	}
}

// 向指定登录用户发送多个消息
func (this *TcpMessage) SendMsgArrToUser(msgArr []*ProtoModel.MessageProto, uid string) {
	for _, messageProto := range msgArr {
		this.SendMsgToUser(messageProto, uid)
	}
}

// 向指定登录用户下某个设备发送消息
func (this *TcpMessage) SendMsgToFd(msg *ProtoModel.MessageProto, Cid string) {

	this.SendMsgToConn(msg, Cid)
}

// 向指定登录用户下某个设备发送消息
func (this *TcpMessage) SendMsgArrToFd(msgArr []*ProtoModel.MessageProto, Did string) {
	for _, messageProto := range msgArr {
		this.SendMsgToFd(messageProto, Did)
	}
}

// 向指定登录用户下某个设备发送消息
func (this *TcpMessage) SendMsgArrToUserFd(msgArr []*ProtoModel.MessageProto, uid string, Did string) {
	for _, messageProto := range msgArr {
		this.SendMsgToUidDid(messageProto, uid, Did)
	}
}

// 向指定房间发送消息 uid 升级成房间编号。
func (this *TcpMessage) SendMsgToRoom(msgArr []*ProtoModel.MessageProto, roomNo string) {

	userConnWs := global2.Ws_.RoomList[roomNo]
	if userConnWs != nil {
		for _, conn := range userConnWs {
			for _, messageProto := range msgArr {
				this.SendMsgToConn(messageProto, conn)
			}
		}
	}

	userConnTcp := global2.Tcp_.RoomList[roomNo]
	if userConnTcp != nil {
		for _, conn := range userConnTcp {
			for _, messageProto := range msgArr {
				this.SendMsgToConn(messageProto, conn)
			}
		}
	}
}

// 向指定登录用户发送消息
func (this *TcpMessage) SendMsgToUser(messageProto *ProtoModel.MessageProto, uid string) {
	userConnWs := global2.Ws_.UserList[uid]
	for _, conn := range userConnWs {
		this.SendMsgToConn(messageProto, conn)
	}
	userConnTcp := global2.Tcp_.UserList[uid]
	for _, conn := range userConnTcp {
		this.SendMsgToConn(messageProto, conn)
	}
}
