package mapper

type FangUser struct {
	Uid    int
	Nick   string
	Avatar string
	FangId int
	Times  int
}
