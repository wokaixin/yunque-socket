package service

import (
	"bytes"
	"errors"
	global2 "src/global"
	"src/model/ProtoModel"
)

type I interface {
	//// 向所有登录用户广播消息
	//SendMsgToUsers()
	//// 向所有登录用户广播消息
	//SendMsgToUserAll()
	//// 向指定多个连接推送消息
	//SendMsgToUsersConns()
	////向一个链接发消息
	//SendMsgToConn()
	////向指定登录用户发送消息
	//SendMsgToUser()
	//推送到指定链接数据
	Send(data []byte, connId string)
	//向所有人推送数据
	SendAll(data []byte)
	//向指定用户推送数据
	SendUid(data []byte, uid string)
	//向指定用户下的某个设备id推送数据
	SendUidDid(data []byte, uid string, did string)
	//向多个用户推送数据
	SendUids(data []byte, uids []string)
}
type Service struct {
}

func (*Service) TcpSend(connId string, msg []byte) error {
	conn := global2.Tcp_.GetConnListVal(connId)
	if conn == nil {
		return errors.New("链接不存在")
	}
	return conn.WriteMes(msg)

}
func (*Service) WsSend(connId string, msg []byte) error {
	conn := global2.Ws_.GetConnListVal(connId)
	if conn == nil {
		return errors.New("链接不存在")
	}
	return conn.WriteMes(msg)
}

// 向指定链接推送消息
func (this *Service) SendPing(connId string, tp string) {
	this.SendNew(ProtoModel.DataTypeProto(ProtoModel.DataTypeProto_PING.Number()), []byte(ProtoModel.DataTypeProto_PONG.String()), connId, tp)
}

// 向指定链接推送消息
func (this *Service) SendPong(connId string, tp string) {
	this.SendNew(ProtoModel.DataTypeProto(ProtoModel.DataTypeProto_PONG.Number()), []byte(ProtoModel.DataTypeProto_PONG.String()), connId, tp)
}

// 向指定链接推送消息
func (this *Service) Send(datatype ProtoModel.DataTypeProto, data []byte, connId string) {
	msg := bytes.Buffer{}
	msg.WriteByte(byte(datatype.Number()))
	msg.Write(data)
	this.WsSend(connId, msg.Bytes())
	this.TcpSend(connId, msg.Bytes())
} // 向指定链接推送消息
func (this *Service) SendNew(datatype ProtoModel.DataTypeProto, data []byte, connId string, tp string) {
	msg := bytes.Buffer{}
	msg.WriteByte(byte(datatype.Number()))
	msg.Write(data)
	this.WsSend(connId, msg.Bytes())
	this.TcpSend(connId, msg.Bytes())
}

// 向某个链接推送
func Send(s I, data []byte, connId string) {
	s.Send(data, connId)
}

// 向所有人推送
func (this *Service) TcpSendAll(data []byte) {
	userList := global2.Tcp_.GetUserList()
	for _, userConns := range userList {
		//向所有已登录绑定的用户发消息
		for _, conn := range userConns {
			this.TcpSend(conn, data)
		}
	}
}

// 向所有人推送
func (this *Service) WsSendAll(data []byte) {
	userList := global2.Ws_.GetUserList()
	for _, userConns := range userList {
		//向所有已登录绑定的用户发消息
		for _, conn := range userConns {
			this.WsSend(conn, data)
		}
	}
}

// 向所有人推送
func (this *Service) SendAll(datatype ProtoModel.DataTypeProto, data []byte) {
	msg := bytes.Buffer{}
	msg.WriteByte(byte(datatype.Number()))
	msg.Write(data)
	this.WsSendAll(msg.Bytes())
	this.TcpSendAll(msg.Bytes())

}
func SendAll(s I, data []byte) {
	s.SendAll(data)
}

// 向指定链接推送消息
func SendUid(s I, data []byte, uid string) {
	s.SendUid(data, uid)
}

// 向指定链接推送消息
func (this *Service) WsSendUid(data []byte, uid string) {
	userConns := global2.Ws_.GetUserListVal(uid)
	if userConns == nil {
		return
	}
	//向所有已登录绑定的用户发消息
	for _, conn := range userConns {
		this.WsSend(conn, data)
	}
}

// 向指定链接推送消息
func (this *Service) TcpSendUid(data []byte, uid string) {
	userConns := global2.Tcp_.GetUserListVal(uid)
	if userConns == nil {
		return
	}
	//向所有已登录绑定的用户发消息
	for _, conn := range userConns {
		this.TcpSend(conn, data)
	}
}

// 向指定链接推送消息
func (this *Service) SendUid(datatype ProtoModel.DataTypeProto, data []byte, uid string) {
	msg := bytes.Buffer{}
	msg.WriteByte(byte(datatype.Number()))
	msg.Write(data)

	this.TcpSendUid(msg.Bytes(), uid)
	this.WsSendUid(msg.Bytes(), uid)
}

// 向指多个用户推送消息
func (this *Service) SendUids(datatype ProtoModel.DataTypeProto, data []byte, uids []string) {
	for _, uid := range uids {
		this.SendUid(datatype, data, uid)
	}
}

// 向指多个用户推送消息
func SendUids(s I, data []byte, uids []string) {
	s.SendUids(data, uids)
}

// 向指定链接推送消息
func SendUidDid(s I, data []byte, uid string, did string) {
	s.SendUidDid(data, uid, did)
}

// 向指定链接推送消息
func (this *Service) TcpSendUidDid(data []byte, uid string, Did string) {
	userConns := global2.Tcp_.GetUserListVal(uid)
	if userConns == nil {
		return
	}
	//向所有已登录绑定的用户发消息
	for _, connid := range userConns {
		userConn := global2.Ws_.GetConnListVal(connid)
		if userConn.Did == Did {
			this.TcpSend(connid, data)
		}
	}
}

// 向指定链接推送消息
func (this *Service) WsSendUidDid(data []byte, uid string, Did string) {
	userConns := global2.Ws_.GetUserListVal(uid)
	if userConns == nil {
		return
	}
	//向所有已登录绑定的用户发消息
	for _, connid := range userConns {
		userConn := global2.Ws_.GetConnListVal(connid)
		if userConn.Did == Did {
			this.WsSend(connid, data)
		}
	}
}

// 向指定链接推送消息
func (this *Service) SendUidDid(datatype ProtoModel.DataTypeProto, data []byte, uid string, Did string) {
	msg := bytes.Buffer{}
	msg.WriteByte(byte(datatype.Number()))
	msg.Write(data)
	//向所有已登录绑定的用户发消
	this.WsSendUidDid(msg.Bytes(), uid, Did)
	this.TcpSendUidDid(msg.Bytes(), uid, Did)
}
