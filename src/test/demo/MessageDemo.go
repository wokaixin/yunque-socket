package demo

import (
	"bytes"
	"errors"
	"fmt"
	"google.golang.org/protobuf/proto"
	"src/connection"
	"src/global"
	"src/model/ProtoModel"
	"time"
	"unsafe"
)

type MessageModel_ struct {
	name      string
	DataType  ProtoModel.DataTypeProto
	Message   ProtoModel.MessageProto
	Sendbody  ProtoModel.SendBodyProto
	ReplyBody ProtoModel.ReplyBodyProto
	Value     string
}

var v *MessageModel_

func MessageModel() *MessageModel_ {
	//v = New()
	return v
}

/**
* @author: yichen
* @Date: 2023/9/11 0011
* @Description: 消息模版推送使用示例
* @return:
**/
func (*MessageModel_) PushMessageModel(tp ProtoModel.DataTypeProto, msgdata []byte) []byte {

	data := msgdata
	switch tp {
	case ProtoModel.DataTypeProto_PONG:
		toMsg := bytes.NewBuffer([]byte{uint8(ProtoModel.DataTypeProto_PONG.Number())})
		if msgdata == nil {
			data = []byte(ProtoModel.DataTypeProto_PONG.String())
		}
		toMsg.Write(data)
		return toMsg.Bytes()
	case ProtoModel.DataTypeProto_PING:
		toMsg := bytes.NewBuffer([]byte{uint8(ProtoModel.DataTypeProto_PING.Number())})
		if msgdata == nil {
			data = []byte(ProtoModel.DataTypeProto_PING.String())
		}
		toMsg.Write(data)
		return toMsg.Bytes()
	case ProtoModel.DataTypeProto_MESSAGE:
		msg := &ProtoModel.MessageProto{
			Id:       1,
			Content:  make(map[string]string),
			Action:   "chat",
			Sender:   "1047",
			Receiver: "1026",
			Extra:    make(map[string]string),
			Title:    "新消息",
			Image:    "",
			SendTime: time.Now().Unix(),
		}
		toMsg := bytes.NewBuffer([]byte{uint8(ProtoModel.DataTypeProto_MESSAGE.Number())})
		if msgdata == nil {
			data, _ = proto.Marshal(msg)
		}

		toMsg.Write(data)
		return toMsg.Bytes()
	case ProtoModel.DataTypeProto_SENT:
		msg := &ProtoModel.SendBodyProto{
			Key:       "client_bind",
			Timestamp: time.Now().Unix(),
			Data:      make(map[string]string),
		}
		toMsg := bytes.NewBuffer([]byte{uint8(ProtoModel.DataTypeProto_SENT.Number())})
		if msgdata == nil {
			data, _ = proto.Marshal(msg)
		}
		toMsg.Write(data)
		return toMsg.Bytes()
	case ProtoModel.DataTypeProto_REPLY:
		msg := &ProtoModel.ReplyBodyProto{
			Key:  "client_bind",
			Code: "200",
			Data: make(map[string]string),
		}
		toMsg := bytes.NewBuffer([]byte{uint8(ProtoModel.DataTypeProto_REPLY.Number())})
		if msgdata == nil {
			data, _ = proto.Marshal(msg)
		}
		toMsg.Write(data)
		return toMsg.Bytes()
	}
	return nil
}
func (this *MessageModel_) SendMessage(tp ProtoModel.DataTypeProto, messageProto ProtoModel.MessageProto) {
	fmt.Print(messageProto.Content)
	//找到用户的链接ID
	conId := global.Get().UserList[messageProto.Receiver]

	ws1 := *(**connection.Ws)(unsafe.Pointer(&conId))

	msg := bytes.Buffer{}
	msg.Write([]byte{byte(tp.Number())})
	data, _ := proto.Marshal(&messageProto)
	msg.Write(data)
	err := ws1.WriteMes(msg.Bytes())
	if err != nil {

	}
}

/**
* @author: yichen
* @Date: 2023/9/11 0011
* @Description: 消息模版接收使用示例
* @return:
**/
func (this *MessageModel_) OnMessageModel(tp ProtoModel.DataTypeProto, msgdata []byte) (*MessageModel_, error) {
	m := &MessageModel_{}
	m.DataType = tp
	switch tp {
	case ProtoModel.DataTypeProto_PONG:

		return m, nil
	case ProtoModel.DataTypeProto_PING:

		return m, nil
	case ProtoModel.DataTypeProto_MESSAGE:

		err := proto.Unmarshal(msgdata, &m.Message)
		if err != nil {
			return nil, err
		}
		return m, nil
	case ProtoModel.DataTypeProto_SENT:

		err := proto.Unmarshal(msgdata, &m.Sendbody)
		if err != nil {
			return nil, err
		}
		return m, nil
	case ProtoModel.DataTypeProto_REPLY:

		err := proto.Unmarshal(msgdata, &m.ReplyBody)
		if err != nil {
			return nil, err
		}
		return m, nil
	}
	return nil, errors.New("非法数据")
}
