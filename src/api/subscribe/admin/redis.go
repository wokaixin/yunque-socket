package admin

import (
	"log"
	"src/handler"
)

type Redis struct {
}

func NewRedis() *Redis {
	return &Redis{}
}

var RedisS *Redis
var idx int64

// 接收订阅消息回调函数
func (*Redis) SubChat(channelName string, msg []byte) {
	idx++
	log.Println(idx, "redis 订阅接收到消息")
	handler.NewSubHandler().SubHandler(channelName, msg)
}
