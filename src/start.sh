#!/bin/bash
#执行方法 start|stop
#sh ./start.sh -a start -n yunque -d
mulu=$(dirname $(readlink -f $0))
# 执行方法：-a start|stop
a='start'
# 执的版本号, 例如：-v 2.2.0
v=''
#-n = 启动文件名
n='yunque'
#-d=环境
d=''
#日志目錄
logdir=logs
[ ! -d ${logdir} ] && mkdir -p ${logdir}
#执行名字例如：-n ruoyi-gateway
# 'ruoyi-modules-job' 'ruoyi-visual-monitor'
# arr=('ruoyi-gateway' 'ruoyi-auth' 'ruoyi-modules-system' 'ruoyi-modules-job' 'ruoyi-modules-gen' 'ruoyi-visual-monitor' 'ruoyi-modules-file' 'beiyao-zt-erp')
arr=()
noJavaArr=('yunque' 'yunque2')

while getopts ":v:n:a:d:" opt
do
	case $opt in
		n)
		n=$OPTARG
		arr=($OPTARG)
		echo "当前操作应用：$OPTARG"
		;;
		v)
		v=$OPTARG
		echo "版本号：$OPTARG"
		;;
		a)
		a=$OPTARG
		echo "操作方法：$OPTARG"
		;;
		d)
		d=$OPTARG
		echo "启动环境：$OPTARG"
		;;
		?)
		echo "未知参数,-v 2.0.0 -n nginx -i start|stop -d prod"
		exit 1;;
	esac
done

if [ ! -n "$v" ]; then
 	for ((i=0; i<${#arr[@]};i++))
	do
	arr[$i]=${arr[i]};
    echo ${arr[i]};
	done
else
	for ((i=0; i<${#arr[@]};i++))
	do
	arr[$i]=${arr[i]}-$v;
    echo ${arr[i]};
	done
#  echo "NOT NULL"
fi


function stopjar(){
	PID=$(ps -ef | grep $1 | grep -v grep | awk '{ print $2 }')
	if [ -z "$PID" ]
	then
	echo $1 is already stopped
	else
	echo "stop $1 kill -9 $PID"
	kill -9 $PID
	fi
}
function inNoJavaArr(){
    val=$1
    # arr2=$noJavaArr
     # 判断数组元素是否包含"banana"
    for ((i=0; i<${#noJavaArr[@]};i++))
    do
     element=${noJavaArr[i]}
        if [[ "$element" == "$val" ]]; then
            echo "数组包含元素$val"
            return 1
            break
        fi
    echo "noJavaArr-" ${noJavaArr[i]} "val:"$val;
    done
    return 0
}
function startjar(){
     inNoJavaArr  $1
    if [ $? -eq 1 ]
    then
        stopjar $mulu/$1
        echo "start nohup  $mulu/$1 -active=$d >$mulu/logs/$1$d.out 2>&1 &"
        nohup  $mulu/$1 -active=$d >$mulu/logs/$1.out 2>&1 &
    else
    stopjar $mulu/$1.jar
    echo "start nohup java -Dfile.encoding=utf-8 -Duser.timezone=GMT+8  -Dspring.profiles.active=$d  -jar -Xmx512M  $mulu/$1.jar  >$mulu/logs/$1.out 2>&1 &"
	nohup java -Dfile.encoding=utf-8 -Duser.timezone=GMT+8   -Dspring.profiles.active=$d -jar -Xmx302M $mulu/$1.jar >$mulu/logs/$1.out 2>&1 &
	fi

}
function stop(){
		length=${#arr}
	# for 遍历
	for item in ${arr[*]}
	do
	 echo "stop" $item
	 stopjar $item
	done
}

function start(){
	length=${#arr}
	echo "开始服务：$length"
	# for 遍历
	for item in ${arr[*]}
	do

	 startjar $item
	done
}
echo "$a $d"
$a
# 日志太大解決
#2、手动输入命令，保留nohup.out文件多少行
#
#例如保留10000行，可如下操作：
#
#log=`tail -n 10000 nohup.out`;
#echo "$log" > nohup.out
#
#3、写定时任务，定时清理nohup.out文件
#
#4、转移nohup.out文件
#
#通过定时任务脚本，执行任务，例如每隔1周，转移日志文件到指定目录。
#
#5、cat /dev/null >nohup.out 该命令会清理掉日志内容，后续程序会继续写该文件
#
#    可以使用split定时分割日志，到其它目录下的文件，然后执行以上命令
#6、只记录错误日志
#
#    nohup java -jar *.jar >/dev/null 2>error.log 2>&1 &
#
#7、不记录任何日志
#
#    nohup java -jar *.jar >/dev/null  2>&1 &