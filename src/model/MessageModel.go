package model

// 接收消息模版
type MessageModel struct {
	Msg  map[string]interface{}
	Type string
	Act  string
}

// 发送消息模版
type MsgResult struct {
	//Token string `json:"token"`
	// Msg    string `json:"msg"`
	Msg struct {
		Time  string `json:"time"`
		Id    int64  `json:"id"`
		Uid   string `json:"uid"`
		Touid string `json:"touid"`
		Msg   string `json:"msg"`
	} `json:"msg"`
}
