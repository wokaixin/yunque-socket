package mapper

// 用户数据
type User struct {
	Uid    string `json:"uid"  description:"用户ID唯一" xorm:"varchar(32) id pk <- unique comment('姓名')"`
	Avatar string `json:"avatar" description:"头像"`
	//昵称
	Nickname   string `json:"nickname" description:"昵称"`
	Username   string `json:"username"  description:"用户帐户唯一"`
	Password   string `json:"password"  description:"用户密码"`
	Token      string `json:"token" description:"token令牌"`
	CreateTime string `json:"createTime" description:"创建时间" xorm:"<- comment('创建时间')"`
	UpdateTime string `json:"updateTime" description:"更新时间" xorm:"<- comment('创建时间')"`
}

//	func (u User) TableName() string {
//		return  "user"
//	}
func (u User) Verify() {

}
