# 分割日志计划任务
logname=$1
#第2个参数是清除几天之前的日志
delday=7
if [ -n "$2" ]; then
  delday=$2
fi
#第三个参数 存放日志的目录
logdir=logs
if [ -n "$3" ]; then
  logdir=$3
fi

mulu=$(dirname $(readlink -f $0))/${logdir}/
current_date=`date -d "-1 day" "+%Y%m%d_%H_%M_%S"`   #列出时间
newlogs=${mulu}${logname}_${current_date}
oldlog=${mulu}${logname}.out
echo "生成新日志名"$newlogs
echo "清空旧日志"$oldlog
echo "日志所在二级目录"$logdir
echo "清理二级目录 "${logdir}" 下历史"$delday"天前的数据"
do_split () {
    [ ! -d ${logdir} ] && mkdir -p ${logdir}
    split -b 10m -d -a 4 ${oldlog}  ${newlogs}  #切分10兆每块至logs文件中，格式为：nohup-xxxxxxxxxx
    if [ $? -eq 0 ];then
        echo "Split is finished!"
    else
        echo "Split is Failed!"
        exit 1
    fi
}

do_del_log() {
    find ${mulu} -type f -ctime +${delday} | xargs rm -rf #清理7天前创建的日志
    cat /dev/null > ${oldlog} #清空当前目录的nohup.out文件
}

if do_split ;then
    do_del_log
    echo "nohup is split Success"
else
    echo "nohup is split Failure"
    exit 2
fi
