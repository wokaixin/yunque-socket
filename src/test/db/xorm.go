package db

// 数据库 orm 中间件
// 文档地址
import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"github.com/prometheus/common/log"
	"github.com/spf13/viper"
	"strings"
)

type User struct {
	Userid int64
	Name   string
	Token  string
}
type FangUser struct {
	Times int
	Uid   int
	Id    int
}

func main() {

	engine, err := xorm.NewEngine("mysql", "yingyin:yingyin@/yingyin")
	if err != nil {
		panic("failed to connect to database")
	}
	user := &User{Token: "2c40ea7e0d361fb9c39b3b974043987d"}
	//查询方法
	has, err := engine.Get(user)
	if has {
		println(user.Name)
	}
	names := []string{"1313", "1314", "1312"}

	var fangUser FangUser
	fangUser.Times++
	inClauseStr := strings.Join(names, ",")
	//e, err := engine.In("id", names).Update(fangUser)
	// 执行原生SQL查询
	engine.SQL("UPDATE `fang_user` SET `times` = `times` + ? WHERE `fang_id` = ? AND `on_line` = ? AND `uid` IN  (?)", 5, 233, 1, inClauseStr)
	log.Error("sqlstr", names, 123, "roomNo")
	update_on_line_time_fang_user := viper.GetBool("heartbeat.is-update-on-line-time-fang-user")
	sql := viper.GetString("heartbeat.update-on-line-time-fang-user-sql")
	log.Info(update_on_line_time_fang_user, sql)
	// 自动创建数据库表结构
	//err = engine.Sync2(new(User))
	//if err != nil {
	//	panic("failed to create table")
	//}
	//
	//// 创建新用户记录
	//user := User{Name: "John Doe", Token: "john@example.com"}
	//affected, err := engine.Insert(&user)
	//if err != nil || affected == 0 {
	//	panic("failed to create user")
	//}
}
