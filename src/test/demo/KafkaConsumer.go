package demo

import (
	"encoding/json"
	"fmt"
	"github.com/IBM/sarama"
	"src/model"
)

// mq接收过来的消息 处理数据结构发送到用户
func SendRouterMq(result model.MessageModel) {
	fmt.Println("result:", result)
	//switch result.Type {
	//case "chat":
	//	// 聊天
	//	//将map数据转化为JSON格式字符串
	//	jsonBytes, err3 := json.Marshal(result.Msg)
	//	if err3 != nil {
	//		fmt.Println(" JSON 转换失败 Error:", err3)
	//	}
	//	switch result.Act {
	//	case "toUid":
	//		if result.Msg["touid"] != nil {
	//			SendMsgToUser(string(jsonBytes), result.Msg["touid"].(string))
	//		}
	//
	//	case "toConn":
	//		SendMsgToAllConn(string(jsonBytes))
	//	}
	//
	//default:
	//
	//}
}

// kafka 统一接收服务端消息，转发给用户端
func KafkaConsumer(pc *sarama.PartitionConsumer, msg *sarama.ConsumerMessage) *sarama.ConsumerMessage {
	fmt.Println("Callbake", msg)
	if pc == nil {
		return msg
	}

	var result model.MessageModel
	err2 := json.Unmarshal(msg.Value, &result)
	if err2 != nil {
		return nil
	}
	SendRouterMq(result)
	return msg
}
