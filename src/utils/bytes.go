package utils

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"testing"
)

var buf bytes.Buffer

func TestName(t *testing.T) {
	// 写入一个字节
	err := buf.WriteByte('a')
	if err != nil {
		fmt.Println(err)
		return
	}

	// 写入一个字符串
	_, err = buf.WriteString("b")
	if err != nil {
		fmt.Println(err)
		return
	}

	// 写入一个无符号整数
	var u uint32 = 12345
	err = binary.Write(&buf, binary.LittleEndian, u)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 读取一个字节
	b, err := buf.ReadByte()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b)) // a

	// 读取一个字符串
	s, err := buf.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(s) // b

	// 读取无符号整数
	var u2 uint32
	err = binary.Read(&buf, binary.LittleEndian, &u2)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(u2) // 12345

	// 拷贝到标准输出中
	_, err = io.Copy(os.Stdout, &buf)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 读取到 []byte 中，并以字符串输出
	data, err := io.ReadAll(&buf)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(data))
}
