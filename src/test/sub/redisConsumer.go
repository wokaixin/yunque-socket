package test

import (
	"fmt"
	"github.com/go-redis/redis"
	"time"
)

func connRedis() {
	client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "123456", // 如果没有设置密码，则为空字符串
		DB:       0,        // 使用默认数据库
	})

}

// Golang：通过Redis消息订阅
func subs2() {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "123456", // 如果没有设置密码，则为空字符串
		DB:       0,        // 使用默认数据库
	})
	defer client.Close()
	now := time.Now()
	// fmt.Println(now.Unix()) // 1565084298 秒fmt.Println(now.UnixNano()) // 1565084298178502600 纳秒fmt.Println(now.UnixNano() / 1e6) // 1565084298178 毫秒
	fmt.Println("end【0 】——————————开始接收订阅消息—————————!")
	fmt.Println(now.UnixNano() / 1e6)
	fmt.Println(now.Unix())
	/// 订阅channel1这个channel
	sub := client.Subscribe("channel1")
	//// 取消订阅
	//submodel.Unsubscribe("channel1")
	// submodel.Channel() 返回go channel，可以循环读取redis服务器发过来的消息
	var b = 1
	for m := range sub.Channel() {
		if b%50000 == 0 {
			//fmt.Println("run【", b, "】——————————接收订阅消息中—————————!")
			fmt.Println("耗时秒", (time.Now()).Unix()-now.Unix(), "耗时毫秒", ((time.Now()).UnixNano()/1e6)-(now.UnixNano()/1e6))
			fmt.Println("run【", b, "】订阅：", m)
		}
		//if b == 100000 {
		//	fmt.Println("耗时秒", (time.Now()).Unix()-now.Unix())
		//	fmt.Println("耗时毫秒", ((time.Now()).UnixNano()/1e6)-(now.UnixNano()/1e6))
		//	fmt.Println("end【", b, "】——————————接收订阅完毕—————————!")
		//}
		b++
		// 打印收到的消息
		//fmt.Println(msg.Channel)
		//fmt.Println(msg.Payload)
	}
	//fmt.Println("耗时秒", (time.Now()).Unix()-now.Unix())
	//fmt.Println("耗时毫秒", ((time.Now()).UnixNano()/1e6)-(now.UnixNano()/1e6))
	//fmt.Println("end【", b, "】——————————接收订阅完毕—————————!")
}
