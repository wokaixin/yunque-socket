package test

import (
	"database/sql"
	"fmt"
	"log"
	"src/connection"
	global2 "src/global"
	"strconv"
)

// //var MyGlobal = &global.MyGlobal{}
//
// // 向所有链接广播消息
//
//	func sendAllCon(w http.ResponseWriter, r *http.Request) {
//		//fmt.Println(r)
//		//global.Update()
//		//fmt.Println(global.Get())
//	}
//
//	func main() {
//		var mes []ProtoModel.MessageProto
//		mes = append(mes, ProtoModel.MessageProto{Action: "XdfdsfdsdfdsX", Image: "1"})
//		mes = append(mes, ProtoModel.MessageProto{Action: "XXxxxxxxxxxxfeeXX", Image: "1"})
//		subpack := (&service.Sub{Sid: "xxfd", Uid: "1084", Cid: int64(12645)}).Packet().Message(mes)
//
//		buf := bytes.NewBuffer(subpack.Buffer.Bytes())
//		var data = []ProtoModel.MessageProto{}
//		tp, _ := buf.ReadByte()
//		dttype, _ := buf.ReadByte()
//		// 读取一个字符串
//		uid, err := buf.ReadBytes('\n')
//		if err != nil {
//			fmt.Println(err)
//			return
//		}
//		fmt.Println("uid:", "---", string(uid), "\n")
//		sid, err := buf.ReadBytes('\n')
//		if err != nil {
//			fmt.Println(err)
//			return
//		}
//		fmt.Println("sid:", "---", string(sid), "\n")
//		cid, err := buf.ReadBytes('\n')
//		if err != nil {
//			fmt.Println(err)
//			return
//		}
//		fmt.Println("cid:", "---", utils.BytesToInt64(cid), "\n")
//
//		fmt.Println("tp:", tp, ";dttype:", dttype)
//		dt := buf.Bytes()
//		//fmt.Println("--------\n")
//		err = json.Unmarshal(dt, &data)
//		if err != nil {
//			return
//		}
//		fmt.Println(data)
//		return
//		//config.Init()
//		//MyGlobal.Init()
//		//http.HandleFunc("/test", sendAllCon)
//		config.Init()
//		//fmt.Println(global.Get())
//		//config.Router()
//		//fmt.Println(viper.GetStringSlice("kafka.brokerIpPort")[0])
//		//fmt.Println(viper.GetString("server.http.port"))
//		fmt.Println(viper.AllSettings())
//		fmt.Println(viper.GetStringSlice("receive.kafka.brokerIpPort"))
//		fmt.Println(viper.GetStringSlice("receive.subscribe.conf.addr"))
//		//
//		//fmt.Println("test")
//		//
//		//log.Fatal(http.ListenAndServe(":8090", nil))
//	}
//
// // 1 推送类型 byte 1 全推送|2 多uid推，3 单uid推 4 单uid下设备id推 5 多消息逐条推(根据每条接收人uid 遍历推)
// // 2 消息类型 byte 1 推功能 2 推聊天
// // 3 服务器id str
// // 4 用户uid str
// // 5 用户设备id int64
// // 6 消息体
//
// //单发   2服务器id  [消息模版]
type user struct {
	id   int
	name string
	age  int
}

func main() {
	global2.WsInit()
	global2.TcpInit()
	for i := 0; i < 100000; i++ {

		//defer func() {
		//	log.Println("defer", i)
		//}()
		k := strconv.Itoa(i)
		go func() {
			value := &connection.Ws{Id: "ws1_" + k}
			global2.Ws_.UpdateConnList(k, value)
			v2 := global2.Ws_.GetConnListVal(k)
			global2.Ws_.DelConnList(k)
			if v2 != nil {
				log.Println(v2.Id)
			}
		}()
		go func() {
			value := &connection.Ws{Id: "ws2_" + k}
			global2.Ws_.UpdateConnList(k, value)
			v2 := global2.Ws_.GetConnListVal(k)
			global2.Ws_.DelConnList(k)
			if v2 != nil {
				log.Println(v2.Id)
			}
		}()
		go func() {
			value := &connection.Ws{Id: "ws3_" + k}
			global2.Ws_.DelConnList(k)
			global2.Ws_.UpdateConnList(k, value)
			v2 := global2.Ws_.GetConnListVal(k)
			if v2 != nil {
				log.Println(v2.Id)
			}
		}()

	}

	//fmt.Printf(viper.GetString("db.mysql"))
	//log.Println(viper.GetString("client.msg-fmt"))
	//mysql := utils.NewMysqlClient("x", nil)
	//userinfo := user{}
	//
	//QueryRow(userinfo, mysql.Client)

}
func QueryRow(queryUser user, db *sql.DB) {
	sqlStr := "SELECT id,name,age from user WHERE 1=1 AND  id = ?"
	row := db.QueryRow(sqlStr, queryUser.id)
	var u user
	//然后使用Scan()方法给对应类型变量赋值，以便取出结果,注意传入的是指针
	err := row.Scan(&u.id, &u.name, &u.age)
	if err != nil {
		fmt.Printf("获取数据错误, err:%v\n", err)
		return
	}
	fmt.Printf("查询数据成功%#v", u)
}
