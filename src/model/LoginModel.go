package model

import "src/mapper"

//// 这里指定了User2的表名为custom_user_table
//func (u *User) TableName() string {
//	return core.Table{Name: "user"}
//}

//func (u *User) Mapper() string {
//	return core.SameMapper{}.Table2Obj("user")
//}

// 注册用户
type LoginModelReg struct {
	Code string `json:"code" description:"验证码"`
	mapper.User
}

// 登录
type LoginModelLogin struct {
	Code string `json:"code" description:"验证码"`
	mapper.User
}

// 注册用户
type TokenModel struct {
	Token  string `json:"token" description:"token令牌"`
	Expiry int64  `json:"expiry" description:"到期时间 时间戳"`
}
type LoginInfo struct {
	Token    TokenModel
	Userinfo mapper.User
}
